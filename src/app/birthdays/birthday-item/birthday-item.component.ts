import { Component, OnInit, Input } from '@angular/core';
import { Birthday } from 'src/app/model/birthday.model';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-birthday-item',
  templateUrl: './birthday-item.component.html',
  styleUrls: ['./birthday-item.component.css']
})
export class BirthdayItemComponent implements OnInit {

  @Input() birthday: Birthday;
  @Input() id: number;

  constructor(private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
  }

  onEdit() {
    this.router.navigate([this.id], {relativeTo: this.route});
  }

  onView() {
    this.router.navigate( ['view/' + this.id], {relativeTo: this.route});
  }

  onDelete() {
    console.log('TODO delete');
  }
}
