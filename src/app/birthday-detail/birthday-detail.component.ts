import { Component, OnInit, Input } from '@angular/core';
import { Birthday } from '../model/birthday.model';
import { BirthdayService } from '../services/birthday.service';
import { ActivatedRoute, Router, Params } from '@angular/router';

@Component({
  selector: 'app-birthday-detail',
  templateUrl: './birthday-detail.component.html',
  styleUrls: ['./birthday-detail.component.css']
})
export class BirthdayDetailComponent implements OnInit {

  id: number;
  birthday: Birthday;

  constructor(private birthdayService: BirthdayService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.route.params.subscribe(
      (params: Params) => {
        this.id = +params['id'];
        this.initForm();
      }
    );
  }

  initForm() {
    this.birthday = this.birthdayService.getBirthday(this.id);
  }

}
