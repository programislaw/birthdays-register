import { Component, OnInit, ViewChild } from '@angular/core';
import { BirthdayService } from '../services/birthday.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Birthday } from '../model/birthday.model';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-birthday-edit',
  templateUrl: './birthday-edit.component.html',
  styleUrls: ['./birthday-edit.component.css']
})
export class BirthdayEditComponent implements OnInit {

  constructor(private birthdayService: BirthdayService, private route: ActivatedRoute, private router: Router) { }

  id: number;
  editMode: boolean;
  birthdayForm: FormGroup;
  subscription: Subscription;

  ngOnInit() {
    this.route.params.subscribe(
      (params: Params) => {
        this.id = +params['id'];
        this.editMode = params['id'] != null;
        this.initForm();
      }
    );
  }

  initForm() {
    if (this.editMode) {
      const birthday = this.birthdayService.getBirthday(this.id);
      this.birthdayForm = new FormGroup({
        'firstname': new FormControl(birthday.firstname, Validators.required),
        'lastname': new FormControl(birthday.lastname, Validators.required),
        'date': new FormControl(birthday.date, Validators.required)
      });
    } else {
      this.birthdayForm = new FormGroup({
        'firstname': new FormControl('', Validators.required),
        'lastname': new FormControl('', Validators.required),
        'date': new FormControl('', Validators.required)
      });
    }
  }

  onSubmit() {
    const birthday = new Birthday(
      this.birthdayForm.value['date'],
      this.birthdayForm.value['firstname'],
      this.birthdayForm.value['lastname']
    );
    if (!this.editMode) {
      this.birthdayService.addBirthday(birthday);
    } else {
      this.birthdayService.editBirthday(this.id, birthday);
    }
      this.navigateBack();
  }

  private navigateBack() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }
}
